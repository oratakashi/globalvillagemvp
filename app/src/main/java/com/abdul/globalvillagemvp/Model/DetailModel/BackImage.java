package com.abdul.globalvillagemvp.Model.DetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackImage {
    @SerializedName("image_1")
    @Expose
    private String image1;
    @SerializedName("image_2")
    @Expose
    private String image2;

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }
}
