package com.abdul.globalvillagemvp.Model.DetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Region {
    @SerializedName("vilage")
    @Expose
    private String vilage;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("regency")
    @Expose
    private String regency;
    @SerializedName("province")
    @Expose
    private String province;

    public String getVilage() {
        return vilage;
    }

    public void setVilage(String vilage) {
        this.vilage = vilage;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getRegency() {
        return regency;
    }

    public void setRegency(String regency) {
        this.regency = regency;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
