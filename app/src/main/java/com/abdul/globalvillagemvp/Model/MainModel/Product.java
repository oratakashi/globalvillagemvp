package com.abdul.globalvillagemvp.Model.MainModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("price_idr")
    @Expose
    private String priceIdr;
    @SerializedName("stock")
    @Expose
    private Integer stock;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("view")
    @Expose
    private Integer view;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("unit")
    @Expose
    private Unit unit;
    @SerializedName("back_images")
    @Expose
    private List<BackImage> backImages = null;
    @SerializedName("front_images")
    @Expose
    private List<String> frontImages = null;
    @SerializedName("publish_date")
    @Expose
    private String publishDate;
    @SerializedName("region")
    @Expose
    private Region region;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getPriceIdr() {
        return priceIdr;
    }

    public void setPriceIdr(String priceIdr) {
        this.priceIdr = priceIdr;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Integer getView() {
        return view;
    }

    public void setView(Integer view) {
        this.view = view;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public List<BackImage> getBackImages() {
        return backImages;
    }

    public void setBackImages(List<BackImage> backImages) {
        this.backImages = backImages;
    }

    public List<String> getFrontImages() {
        return frontImages;
    }

    public void setFrontImages(List<String> frontImages) {
        this.frontImages = frontImages;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
}
