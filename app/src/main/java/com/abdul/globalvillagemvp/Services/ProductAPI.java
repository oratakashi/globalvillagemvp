package com.abdul.globalvillagemvp.Services;

import com.abdul.globalvillagemvp.Model.DetailModel.ResponseDetail;
import com.abdul.globalvillagemvp.Model.MainModel.ResponseProduct;


import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProductAPI {
    @Headers({"userkey: 1234","passkey: 1234"})
    @GET("product")
    Single<ResponseProduct> getProduct(@Query("limit") int limit, @Query("start") int start);

    @Headers({"userkey: 1234","passkey: 1234"})
    @GET("product/{id}")
    Single<ResponseDetail> getDetail(@Path("id") String id);
}
