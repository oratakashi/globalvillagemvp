package com.abdul.globalvillagemvp.Services;

import com.abdul.globalvillagemvp.Model.DetailModel.ResponseDetail;
import com.abdul.globalvillagemvp.Model.MainModel.ResponseProduct;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataServices {

    private static final String BASE_URL = "http://ws2.globalvillage.co.id/";

    private ProductAPI api;

    public DataServices() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        api = retrofit.create(ProductAPI.class);
    }

    public Single<ResponseProduct> getProduct(){
        return api.getProduct(6, 0);
    }

    public Single<ResponseDetail> getDetail(String id){
        return api.getDetail(id);
    }
}
