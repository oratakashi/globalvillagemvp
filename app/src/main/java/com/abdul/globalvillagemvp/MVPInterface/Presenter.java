package com.abdul.globalvillagemvp.MVPInterface;

public interface Presenter {
    void setView(View view);
    void fetch();
    void refresh();
}
