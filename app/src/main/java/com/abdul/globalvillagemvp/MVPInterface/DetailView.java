package com.abdul.globalvillagemvp.MVPInterface;

import com.abdul.globalvillagemvp.MVPInterface.View;

import java.util.ArrayList;
import java.util.HashMap;

public interface DetailView extends View {
    @Override
    void showError();

    @Override
    void setData(ArrayList<HashMap<String, String>> dataModel);

    @Override
    void onProgress();

    @Override
    void onRefresh();

    void setSlider(String[] foto);
}
