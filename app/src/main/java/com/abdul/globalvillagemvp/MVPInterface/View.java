package com.abdul.globalvillagemvp.MVPInterface;

import com.abdul.globalvillagemvp.Model.MainModel.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface View {
    void showError();
    void setData(ArrayList<HashMap<String, String>> dataModel);
    void onProgress();
    void onRefresh();
}
