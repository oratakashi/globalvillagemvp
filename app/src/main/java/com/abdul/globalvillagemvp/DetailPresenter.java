package com.abdul.globalvillagemvp;

import android.util.Log;

import androidx.annotation.Nullable;

import com.abdul.globalvillagemvp.MVPInterface.DetailView;
import com.abdul.globalvillagemvp.MVPInterface.Presenter;
import com.abdul.globalvillagemvp.MVPInterface.View;
import com.abdul.globalvillagemvp.Model.DetailModel.Category;
import com.abdul.globalvillagemvp.Model.DetailModel.ResponseDetail;
import com.abdul.globalvillagemvp.Model.DetailModel.Product;
import com.abdul.globalvillagemvp.Model.DetailModel.Unit;
import com.abdul.globalvillagemvp.Model.DetailModel.User;
import com.abdul.globalvillagemvp.Services.DataServices;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class DetailPresenter implements Presenter {

    @Nullable
    View view;
    DetailView detailView;
    private DataServices services;
    private String id;

    public DetailPresenter(@Nullable View view, DetailView detailView, String id) {
        this.view = view;
        this.detailView = detailView;
        this.id = id;
        services = new DataServices();
        fetch();
    }

    @Override
    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void fetch() {
        detailView.onProgress();
        services.getDetail(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ResponseDetail>() {
                    @Override
                    public void onSuccess(ResponseDetail value) {
                        /**
                         * Define json object
                         */
                        Product product = value.getData();
                        Unit unit = product.getUnit();
                        User user = product.getUser();
                        Category category = product.getCategory();

                        /**
                         * Declare for PhotoSlider and Arraylist Output
                         */
                        ArrayList<HashMap<String, String>> dataModel = new ArrayList<>();
                        HashMap<String, String> model = new HashMap<>();
                        List<String> foto = product.getFrontImages();
                        String[] list_foto = new String[foto.size()];

                        /**
                         * Looping for PhotoSlider
                         */
                        for(int i = 0; i < foto.size(); i++){
                            list_foto[i] = foto.get(i);
                        }

                        /**
                         * Put All Data from Product Model to HashMap
                         */
                        model.put("nama", product.getName());
                        model.put("harga", product.getPriceIdr());
                        model.put("unit", unit.getName());
                        model.put("pemilik", user.getName());
                        model.put("berat", String.valueOf(product.getWeight()));
                        model.put("kategori", category.getName());
                        model.put("deskripsi", product.getDescription());

                        /**
                         * Show data to View
                         */
                        dataModel.add(model);
                        detailView.setData(dataModel);
                        detailView.setSlider(list_foto);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("E.error", e.toString());
                        view.showError();
                    }
                });
    }

    @Override
    public void refresh() {
        fetch();
    }
}
