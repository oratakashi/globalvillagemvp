package com.abdul.globalvillagemvp;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abdul.globalvillagemvp.MVPInterface.DetailView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.snackbar.Snackbar;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;

public class DetailActivity extends AppCompatActivity implements DetailView {

    @BindView(R.id.slide_foto)
    CarouselView slide_foto;
    @BindView(R.id.detail_nama)
    TextView nama;
    @BindView(R.id.detail_harga)
    TextView harga;
    @BindView(R.id.detail_pemilik)
    TextView pemilik;
    @BindView(R.id.detail_berat)
    TextView berat;
    @BindView(R.id.detail_pemesanan)
    TextView pemesanan;
    @BindView(R.id.detail_kategori)
    TextView kategori;
    @BindView(R.id.detail_deskripsi)
    TextView deskripsi;


    ImageListener imageListener;
    SpotsDialog loading;

    DetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();

        ButterKnife.bind(this);
        presenter = new DetailPresenter(this, this, extras.getString("id"));

    }

    @Override
    public void showError() {
        Snackbar.make(getWindow().getDecorView(), "Gagal memuat data!", Snackbar.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void setData(ArrayList<HashMap<String, String>> dataModel) {
        nama.setText(dataModel.get(0).get("nama"));
        harga.setText(dataModel.get(0).get("harga")+"/"+dataModel.get(0).get("unit"));
        pemilik.setText(dataModel.get(0).get("pemilik"));
        berat.setText(dataModel.get(0).get("berat")+" "+dataModel.get(0).get("unit"));
        pemesanan.setText(dataModel.get(0).get("berat")+" "+dataModel.get(0).get("unit"));
        deskripsi.setText(dataModel.get(0).get("deskripsi"));
        loading.hide();
    }

    @Override
    public void onProgress() {
        loading = new SpotsDialog(this, "Memuat data produk");
        loading.setCancelable(false);
        loading.show();
    }

    @Override
    public void onRefresh() {
        presenter.refresh();
    }

    @Override
    public void setSlider(String[] foto) {
        imageListener = (position, imageView) -> Glide.with(getApplicationContext())
                .load(foto[position])
                .centerCrop()
                .error(R.drawable.ic_image)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
        slide_foto.setImageListener(imageListener);
        slide_foto.setPageCount(foto.length);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                Toast.makeText(this, "Menu Share clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.cart_detail:
                Toast.makeText(this, "Menu Cart clicked", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();

        return false;
    }
}
