package com.abdul.globalvillagemvp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.abdul.globalvillagemvp.Adapter.AdapterProduct;
import com.abdul.globalvillagemvp.MVPInterface.View;
import com.abdul.globalvillagemvp.Model.MainModel.Product;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View {

    @BindView(R.id.list_product)
    RecyclerView list_product;

    @BindView(R.id.refresh_product)
    SwipeRefreshLayout refreshLayout;

    ArrayList<HashMap<String, String>> list_data;
    AdapterProduct adapter;

    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Baru Panen");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        list_data = new ArrayList<HashMap<String, String>>();

        presenter = new MainPresenter(this);
        adapter = new AdapterProduct(this, list_data);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        list_product.setLayoutManager(layoutManager);
        list_product.setAdapter(adapter);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.refresh();
            }
        });
    }

    @Override
    public void showError() {
        Snackbar.make(getWindow().getDecorView(), "Gagal memuat data!", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setData(ArrayList<HashMap<String, String>> dataModel) {
        refreshLayout.setRefreshing(false);
        list_data.clear();
        list_data.addAll(dataModel);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onProgress() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void onRefresh() {
        presenter.refresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_utama, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.trans:
                Toast.makeText(this, "Menu Trans clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.people:
                Toast.makeText(this, "Menu Profile clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.cart_main:
                Toast.makeText(this, "Menu Cart clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.fav:
                Toast.makeText(this, "Menu Fav clicked", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }

}
