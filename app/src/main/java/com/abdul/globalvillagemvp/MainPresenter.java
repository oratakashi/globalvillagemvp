package com.abdul.globalvillagemvp;

import android.util.Log;

import androidx.annotation.Nullable;

import com.abdul.globalvillagemvp.MVPInterface.Presenter;
import com.abdul.globalvillagemvp.MVPInterface.View;
import com.abdul.globalvillagemvp.Model.MainModel.ResponseProduct;
import com.abdul.globalvillagemvp.Model.MainModel.Product;
import com.abdul.globalvillagemvp.Model.MainModel.Region;
import com.abdul.globalvillagemvp.Model.MainModel.Unit;
import com.abdul.globalvillagemvp.Model.MainModel.User;
import com.abdul.globalvillagemvp.Services.DataServices;

import java.util.ArrayList;
import java.util.HashMap;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter implements Presenter {
    @Nullable
    View view;
    private  DataServices services;

    public MainPresenter(@Nullable View view) {
        this.view = view;
        services = new DataServices();
        fetch();
    }

    @Override
    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void fetch() {
        view.onProgress();
        services.getProduct()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ResponseProduct>() {
                    @Override
                    public void onSuccess(ResponseProduct value) {
                        ArrayList<Product> data = value.getData();
                        ArrayList<HashMap<String, String>> dataModel = new ArrayList<>();

                        for(Product product: data){
                            HashMap<String, String> model = new HashMap<>();
                            User users = product.getUser();
                            Region region = product.getRegion();
                            Unit unit = product.getUnit();
                            model.put("id", product.getId());
                            model.put("nama", product.getName());
                            model.put("harga", product.getPriceIdr());
                            model.put("pemilik", users.getName());
                            model.put("provinsi", region.getProvince());
                            model.put("kota", region.getRegency());
                            model.put("unit", unit.getName());
                            model.put("foto", product.getImageUrl());
                            dataModel.add(model);
                        }

                        view.setData(dataModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("E.error", e.toString());
                        view.showError();
                    }
                });

    }

    @Override
    public void refresh() {
        fetch();
    }
}
