package com.abdul.globalvillagemvp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.abdul.globalvillagemvp.DetailActivity;
import com.abdul.globalvillagemvp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.ViewHolder> {
    Context contetxt;
    ArrayList<HashMap<String, String>> list_data;

    public AdapterProduct(Context contetxt, ArrayList<HashMap<String, String>> list_data) {
        this.contetxt = contetxt;
        this.list_data = list_data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nama.setText(list_data.get(position).get("nama"));
        holder.pemilik.setText(list_data.get(position).get("pemilik"));
        holder.harga.setText("Rp. "+list_data.get(position).get("harga")+"/"+list_data.get(position).get("unit"));
        holder.alamat.setText(list_data.get(position).get("kota")+", "+list_data.get(position).get("provinsi"));
        Glide.with(contetxt)
                .load(list_data.get(position).get("foto"))
                .centerCrop()
                .error(R.drawable.ic_image)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.foto);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(contetxt, DetailActivity.class);
                i.putExtra("id", list_data.get(position).get("id"));
                contetxt.startActivity(i);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView nama, pemilik, harga, alamat;
        ImageView foto;
        CardView layout;
        public ViewHolder(View itemView){
            super(itemView);
            nama = itemView.findViewById(R.id.produk_nama);
            pemilik = itemView.findViewById(R.id.produk_pemilik);
            harga = itemView.findViewById(R.id.produk_harga);
            alamat = itemView.findViewById(R.id.produk_alamat);
            foto = itemView.findViewById(R.id.produk_foto);
            layout = itemView.findViewById(R.id.layout_produk);
        }
    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }
}
